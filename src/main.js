import * as R from 'ramda';
import * as http from 'axios';

export default function main(context, callback) {
	Promise.all([fetchKnownRepos(context.storage), fetchStarRepos(context.secrets.LANGUAGE)])
		.then(values => {
			var knownRepos = R.head(values);
			var latestRepos = R.last(values);
			var newStars = newStarRepos(knownRepos, latestRepos);
			var repo = {
				knownStars: knownRepos,
				newStars: newStars
			}
			return sendNewStarsMessage(repo, context.secrets.SLACK_WEBHOOK_URL);
		})
		.then(repo => {
			return save(context.storage, repo);
		})
		.then(repo => {
			callback(null, repo);
		})
		.catch(resaon => {
			console.error('ERR!', resaon);
			callback(resaon);
		})
}


function fetchKnownRepos(storage) {
	if (!storage) {
		return Promise.reject('nil storage');
	}
	return new Promise((resolve, reject) => {
		storage.get((error, data) => {
			if (error) {
				reject(error);
			}
			resolve(data || []);
		})
	})
}

function newStarRepos(knownRepos, latestRepos) {
	return R.difference(latestRepos, knownRepos);
}

function sendNewStarsMessage(repo, slackWebhookURL) {
	if (!slackWebhookURL) {
		return Promise.reject('SLACK_WEBHOOK_URL is missing');
	}
	if (slackWebhookURL === 'https://SLACK-WEBHOOK-URL') {
		return Promise.reject('replace `' + slackWebhookURL + '` with correct URL');
	}
	console.log('SLACK-WEBHOOK-URL', slackWebhookURL);
	var post = R.partial(http.post, [slackWebhookURL]);
	var message = R.compose(R.objOf('text'), format);
	var postMessage = R.compose(post, message);
	return Promise.all(R.map(postMessage, repo.newStars))
			.then(R.always(repo))
			.catch(error => {
				console.error('send message failed', error);
				return Promise.reject(error);
			});
}

function format(repo) {
	return '<' + repo.url + '>\n' + repo.description;
}

function save(storage, repo) {
	if (!storage) {
		return Promise.reject('nil storage');
	}

	var knownStars = merge(repo.newStars, repo.knownStars);
	repo.knownStars = knownStars;
	return new Promise((resolve, reject) => {
		storage.set(knownStars, function (error) {
            if (error) {
            	reject(error);	
            }
            resolve(repo);
        });
	})
}

function merge(newStars, knownStars) {
	knownStars = (newStars || []).concat(knownStars || []);
	console.log('merge knownStars', knownStars);
	var maxSize = 200;
	return R.take(maxSize, knownStars);
}

function fetchStarRepos(language) {
	var getRepos = R.path(['data', 'items']);
	var simplifyRepos = R.map(simplify);
	var toSimplifiedRepos = R.compose(simplifyRepos, getRepos);
	var url = 'https://api.github.com/search/repositories?q=sort=stars&order=desc' + (language ? '&language:' + language : '');
	console.log('fetch new stars URL', url);
	return http.get(url)
		.then(toSimplifiedRepos);
}

function simplify(repo) {
	return {
		url: repo.html_url,
		description: short(repo.description)
	}
}

function short(description) {
	var max = 250;
	if (!description || description.length < max) {
		return description;
	}

	return R.take(max, description) + '...';
}