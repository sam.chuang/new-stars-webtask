## New Stars
Discover popular Github projects by send messages to Slack

----

## Usage
1. [Install webtask](https://webtask.io/cli)
2. [Create Slack App and Webhook URL](https://api.slack.com/slack-apps)
	- If haven't created Webhook URL, here are the steps
		- Activate Incoming Webhooks
        ![Activate Incoming Webhooks](/source/images/activate-webhook.png)
		- Add New Webhook to Workspace
        ![Add New Webhook to Workspace](/source/images/add-new-webhook-to-workspace.png)
		- Webhook URL
        ![Webhook URL](/source/images/webhook-url.png)
3. [Prepare webtask secret parameters](https://webtask.io/docs/issue_parameters)
	- SLACK_WEBHOOK_URL **required** ` --secret SLACK_WEBHOOK_URL=https://SLACK-WEBHOOK-URL`
	- LANGUAGE **optional** '--secret LANGUAGE=javascript'
4. [Create scheduled New Stars webtask service](https://webtask.io/docs/cron)
	- `git clone https://gitlab.com/sam.chuang/new-stars-webtask.git`
	- `cd new-stars-webtask`
	- `wt cron create --schedule "0 8 * * *" --tz Asia/Taipei dist/newstars.js  --name newstars --secret SLACK_WEBHOOK_URL=https://SLACK-WEBHOOK-URL --secret LANGUAGE=javascript`
	    - **Remember to replace SLACK-WEBHOOK-URL above**
	    - for --tz option, refer to [timezone](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) 
	![Slack New Stars](/source/images/slack-new-stars.png)