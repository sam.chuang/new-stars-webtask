import resolve from 'rollup-plugin-node-resolve';

export default {
  entry: 'src/main.js',
  format: 'cjs',
  moduleName: 'newstars',
  plugins: [ 
  	resolve({
	  	customResolveOptions: {
	      moduleDirectory: 'node_modules'
	    }
	})
  ],
  external: ['ramda', 'axios'],
  dest: 'dist/newstars.js' // equivalent to --output
};
